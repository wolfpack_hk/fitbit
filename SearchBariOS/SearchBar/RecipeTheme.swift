//
//  RecipeTheme.swift
//  SearchBariOS
//
//  Created by cpuser on 15/3/2020.
//  Copyright © 2020 cpuser. All rights reserved.
//

import UIKit

extension UIColor {
  static let candyGreen = UIColor(red: 67.0/255.0, green: 205.0/255.0, blue: 135.0/255.0, alpha: 1.0)
}
