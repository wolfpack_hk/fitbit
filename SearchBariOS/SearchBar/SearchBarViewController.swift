//
//  SearchBarViewController.swift
//  SearchBariOS
//
//  Created by cpuser on 14/3/2020.
//  Copyright © 2020 cpuser. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SearchBarViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchFooter: SearchFooter!
    @IBOutlet weak var searchFooterBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var keywordTextField: UITextField!
    
    private let cellIdentifier = "ResultTableViewCell"
    
    var dateToMillisecond: Double!
    
    var recipes: [Recipe] = []
    
    var willSearchString: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 116
        
        tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(forName: UIResponder.keyboardWillChangeFrameNotification,
                                       object: nil, queue: .main) { (notification) in
                                        self.handleKeyboard(notification: notification) }
        notificationCenter.addObserver(forName: UIResponder.keyboardWillHideNotification,
                                       object: nil, queue: .main) { (notification) in
                                        self.handleKeyboard(notification: notification) }
        
        keywordTextField.delegate = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    func handleKeyboard(notification: Notification) {
        // 1
        guard notification.name == UIResponder.keyboardWillChangeFrameNotification else {
            searchFooterBottomConstraint.constant = 0
            view.layoutIfNeeded()
            return
        }
        
        guard
            let info = notification.userInfo,
            let keyboardFrame = info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
            else {
                return
        }
        
        // 2
        let keyboardHeight = keyboardFrame.cgRectValue.size.height
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.searchFooterBottomConstraint.constant = self.searchFooterBottomConstraint.constant - keyboardHeight
            self.view.layoutIfNeeded()
        })
    }
    
    
    func searchRecipe(keyword: String, completion: @escaping ([Recipe]?) -> Void) {
        let currentSeconds = Date().timeIntervalSince1970 * 1000.0
        
        
        print("difference: \(currentSeconds - self.dateToMillisecond)")
        
        
        guard keyword == keywordTextField.text, currentSeconds - self.dateToMillisecond >= 500.0 else {
            completion(nil)
            return
        }
        
      Alamofire.request(ApiDispatcher.recipe(keyword))
        .responseJSON { response in
            
          guard response.result.isSuccess,
            let value = response.result.value else {
              print("Error while fetching colors: \(String(describing: response.result.error))")
              completion(nil)
              return
          }
          
          let recipe = JSON(value)["results"].array?.map { json -> Recipe in
            Recipe(title: json["title"].stringValue,
                       href: json["href"].stringValue,
                       ingredients: json["ingredients"].stringValue,
                       thumbnail: json["thumbnail"].stringValue)
          }
          completion(recipe)
      }
    }
}


extension SearchBarViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        searchFooter.setIsFilteringToShow(filteredItemCount:
          recipes.count, of: recipes.count)
        return recipes.count
    }
    
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let displayedItem = recipes[indexPath.row]
    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ResultTableViewCell
    cell.selectionStyle = .none
    cell.titleLabel?.text = displayedItem.title
    cell.ingredientsLabel.text = displayedItem.ingredients
    return cell
  }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.keywordTextField.resignFirstResponder()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let detailVC = storyboard.instantiateViewController(
            withIdentifier: "DetailViewController") as? DetailViewController {
            
            let recipe: Recipe
            recipe = recipes[indexPath.row]
            
            detailVC.recipe = recipe
            
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
    }
}

extension SearchBarViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        if updatedText.count > 0 {
            self.dateToMillisecond = Date().timeIntervalSince1970 * 1000.0
            DispatchQueue.main.asyncAfter(deadline: .now() + DispatchTimeInterval.milliseconds(500)) {
                self.searchRecipe(keyword: self.keywordTextField.text ?? "") { recipes in
                    self.recipes = recipes ?? []
                    self.tableView.reloadData()
                }
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
