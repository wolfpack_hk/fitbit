//
//  Recipe.swift
//  SearchBariOS
//
//  Created by cpuser on 14/3/2020.
//  Copyright © 2020 cpuser. All rights reserved.
//

import Foundation
import ObjectMapper

struct Recipe {
    var title: String?
    var href: String?
    var ingredients: String?
    var thumbnail: String?
}
