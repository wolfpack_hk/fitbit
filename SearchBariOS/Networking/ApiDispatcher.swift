//
//  ApiDispatcher.swift
//  SearchBariOS
//
//  Created by cpuser on 14/3/2020.
//  Copyright © 2020 cpuser. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

public enum ApiDispatcher: URLRequestConvertible {
  enum Constants {
    static let baseURLPath = "http://www.recipepuppy.com"
    static let authenticationToken = ""
  }
  
  case recipe(String)
  
  var method: HTTPMethod {
    switch self {
    case .recipe:
      return .get
    }
  }
  
  var path: String {
    switch self {
    case .recipe:
      return "/api"
    }
  }
  
  var parameters: [String: Any] {
    switch self {
    case .recipe(let searchKeyword):
        return ["q": searchKeyword, "p":"3"]
    }
  }
  
  public func asURLRequest() throws -> URLRequest {
    let url = try Constants.baseURLPath.asURL()
    
    var request = URLRequest(url: url.appendingPathComponent(path))
    request.httpMethod = method.rawValue
    request.setValue(Constants.authenticationToken, forHTTPHeaderField: "Authorization")
    request.timeoutInterval = TimeInterval(10 * 1000)
    
    return try URLEncoding.default.encode(request, with: parameters)
  }
}

