//
//  DetailViewController.swift
//  SearchBariOS
//
//  Created by cpuser on 16/3/2020.
//  Copyright © 2020 cpuser. All rights reserved.
//

import UIKit
import WebKit
import SnapKit

class DetailViewController: UIViewController {
    var webView: WKWebView = WKWebView()
    
    var recipe: Recipe? {
      didSet {
        configureView()
      }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        // Do any additional setup after loading the view.
    }
    
    func configureView() {
        webView = WKWebView(frame: .zero)
        self.view.addSubview(webView)
        
        webView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().inset(self.getSafeAreaInsets().bottom)
            make.left.equalToSuperview().inset(16)
            make.right.equalToSuperview().inset(16)
        }
        
        if let recipe = recipe, let url =  URL(string: recipe.href ?? "") {
            webView.load(URLRequest(url: url))
        }
    }
    
    func getSafeAreaInsets() -> UIEdgeInsets {
        if #available(iOS 11.0, *) {
            if let safeAreaInsets = UIApplication.shared.keyWindow?.safeAreaInsets {
                return safeAreaInsets
            } else {
                return .zero
            }
        } else {
            return .zero
        }
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
